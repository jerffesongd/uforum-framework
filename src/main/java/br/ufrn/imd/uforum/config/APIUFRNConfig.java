package br.ufrn.imd.uforum.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Component
@PropertySource("classpath:apiufrn.properties")
public class APIUFRNConfig {
	
	@Value("${url-base}")
	private String urlBase;

	@Value("${url-autenticacao}")
	private String urlAutenticacao;

	@Value("${url-token}")
	private String urlToken;
	
	@Value("${url-refresh-token}")
	private String urlRefreshToken;
	
	@Value("${url-logout}")
	private String urlLogout;

	@Value("${versao}")
	private String versao;

	@Value("${x-api-key}")
	private String xApiKey;
	
	/**
	 * Formata a URL de autenticacao com a urlRedirect
	 * 
	 * @param urlRedirect
	 * @return
	 */
	public String getUrlAutenticacao(String urlRedirect) {
		return String.format(urlAutenticacao, urlRedirect);
	}
	
	/**
	 * Formata a URL de requisição ao token de acesso com o code e a urlRedirect
	 * 
	 * @param code
	 * @param urlRedirect
	 * @return
	 */
	public String getUrlToken(String code, String urlRedirect) {
		return String.format(urlToken, code, urlRedirect);
	}
	
	/**
	 * Formata a URL de requisição do token refresh
	 * 
	 * @param code
	 * @param urlRedirect
	 * @return
	 */
	public String getUrlRefreshToken(String refreshToken) {
		return String.format(urlRefreshToken, refreshToken);
	}
	
	/**
	 * Formata a URL de logout com a urlRedirect
	 * 
	 * @param urlRedirect
	 * @return
	 */
	public String getUrlLogout(String urlRedirect) {
		return String.format(urlLogout, urlRedirect);
	}

	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	public String getUrlAutenticacao() {
		return urlAutenticacao;
	}

	public void setUrlAutenticacao(String urlAutenticacao) {
		this.urlAutenticacao = urlAutenticacao;
	}

	public String getUrlToken() {
		return urlToken;
	}

	public void setUrlToken(String urlToken) {
		this.urlToken = urlToken;
	}

	public String getUrlRefreshToken() {
		return urlRefreshToken;
	}

	public void setUrlRefreshToken(String urlRefreshToken) {
		this.urlRefreshToken = urlRefreshToken;
	}

	public String getUrlLogout() {
		return urlLogout;
	}

	public void setUrlLogout(String urlLogout) {
		this.urlLogout = urlLogout;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getxApiKey() {
		return xApiKey;
	}

	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}
	
}
