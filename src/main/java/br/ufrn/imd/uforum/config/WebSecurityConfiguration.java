package br.ufrn.imd.uforum.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import br.ufframework.config.WebSecurityConfigurationAbstract;
import br.ufframework.service.AbstractAuthProviderService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurationAbstract {

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/**
		 * permissao das paginas.
		 */
		// TODO Auto-generated method stub
		http
        .authorizeRequests()
			.antMatchers("/**").permitAll();
			
		http.csrf().disable();
		super.configure(http);
	}
		
	@Override
	protected String pageLoginSucess() {
		// TODO Auto-generated method stub
		return "/";
	}

	@Override
	protected String pageLogoutSucess() {
		// TODO Auto-generated method stub
		return "/";
	}

	@Override
	protected AbstractAuthProviderService<?> getAuthProvider() {
		// TODO Auto-generated method stub
		return null;
	}

}
