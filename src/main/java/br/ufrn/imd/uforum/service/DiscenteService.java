package br.ufrn.imd.uforum.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.ufrn.imd.uforum.dto.DiscenteDTO;
import br.ufrn.imd.uforum.dto.UsuarioDTO;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Service
public class DiscenteService extends AbstractAPIUFRNService {

	public List<DiscenteDTO> getDiscentes(UsuarioDTO usuario) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "discente/" + config.getVersao() + "/discentes?sigla-nivel=G&situacao-discente=1&cpf-cnpj=" + usuario.getCpfCnpj();
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<DiscenteDTO[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, DiscenteDTO[].class);
		return Arrays.asList(responseEntity.getBody());
	}

	public DiscenteDTO getDiscenteById(int idDiscente) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "discente/" + config.getVersao() + "/discentes/" + idDiscente;
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<DiscenteDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, DiscenteDTO.class);
		return responseEntity.getBody();
	}
	
}
