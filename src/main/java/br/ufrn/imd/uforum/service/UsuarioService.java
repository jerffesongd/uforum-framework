package br.ufrn.imd.uforum.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.ufrn.imd.uforum.dto.AccessTokenDTO;
import br.ufrn.imd.uforum.dto.UsuarioDTO;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Service
public class UsuarioService extends AbstractAPIUFRNService {

	private UsuarioDTO usuarioLogado;

	public UsuarioDTO getUsuarioLogado() {
		if (usuarioLogado == null) {
			RestTemplate restTemplate = new RestTemplate();
			String url = config.getUrlBase() + "usuario/" + config.getVersao() + "/usuarios/info";
			HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
			ResponseEntity<UsuarioDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, UsuarioDTO.class);
			usuarioLogado = responseEntity.getBody();
		}
		return usuarioLogado;
	}

	public AccessTokenDTO getAccessToken(String code, String urlRedirect) {
		RestTemplate restTemplate = new RestTemplate();
		String url = String.format(config.getUrlToken(code, urlRedirect));
		ResponseEntity<AccessTokenDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, null, AccessTokenDTO.class);
		return responseEntity.getBody();
	}

	public AccessTokenDTO getRefreshToken(String refreshToken) {
		RestTemplate restTemplate = new RestTemplate();
		String url = String.format(config.getUrlRefreshToken(refreshToken));
		ResponseEntity<AccessTokenDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, null, AccessTokenDTO.class);
		return responseEntity.getBody();
	}

}
