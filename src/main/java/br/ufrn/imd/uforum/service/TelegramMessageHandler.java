package br.ufrn.imd.uforum.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufframework.domain.Grupo;
import br.ufframework.telegram.handler.MessageReceiverTemplate;
import br.ufrn.imd.uforum.dto.TurmaDTO;
import br.ufrn.imd.uforum.repository.TurmaDTORepository;

@Service
public class TelegramMessageHandler extends MessageReceiverTemplate {
	
	@Autowired
	private TurmaDTORepository turmaDTORepository;

	@Override
	public Collection<? extends Grupo> consultarGrupos(int idUsuarioExterno) {
		Collection<TurmaDTO> turmas = turmaDTORepository.findTurmasByUser(idUsuarioExterno);
		return turmas;
	}

	@Override
	public String getGrupoCommand() {
		return "/turmas";
	}

	@Override
	public String getDescricaoGrupoCommand() {
		return "Seleciona uma das suas turmas no semestre atual";
	}
	
}
