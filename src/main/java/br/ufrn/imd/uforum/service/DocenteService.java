package br.ufrn.imd.uforum.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.ufrn.imd.uforum.dto.DocenteDTO;
import br.ufrn.imd.uforum.dto.UsuarioDTO;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Service
public class DocenteService extends AbstractAPIUFRNService {
	
	public List<DocenteDTO> getDocentes(UsuarioDTO usuario) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "docente/" + config.getVersao() + "/docentes?cpf=" + usuario.getCpfCnpj();
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<DocenteDTO[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, DocenteDTO[].class);
		return Arrays.asList(responseEntity.getBody());
	}

	public DocenteDTO getDocenteById(Integer idDocente) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "docente/" + config.getVersao() + "/docentes/" + idDocente;
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<DocenteDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, DocenteDTO.class);
		return responseEntity.getBody();
	}
	
}
