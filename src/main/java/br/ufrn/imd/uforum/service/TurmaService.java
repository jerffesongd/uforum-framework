package br.ufrn.imd.uforum.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.ufrn.imd.uforum.dto.DiscenteDTO;
import br.ufrn.imd.uforum.dto.DocenteDTO;
import br.ufrn.imd.uforum.dto.ParticipanteTurmaDTO;
import br.ufrn.imd.uforum.dto.TurmaDTO;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Service
public class TurmaService extends AbstractAPIUFRNService {
	
	public List<TurmaDTO> getTurmas(DiscenteDTO discente) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "turma/" + config.getVersao() + "/turmas?ano=2018&periodo=2&id-discente=" + discente.getIdDiscente();
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<TurmaDTO[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, TurmaDTO[].class);
		return Arrays.asList(responseEntity.getBody());
	}
	
	public List<TurmaDTO> getTurmas(DocenteDTO docente) {
		RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "turma/" + config.getVersao() + "/turmas?ano=2018&periodo=2&id-docente=" + docente.getIdDocente();
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<TurmaDTO[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, TurmaDTO[].class);
		return Arrays.asList(responseEntity.getBody());
	}
	
	public List<ParticipanteTurmaDTO> getParticipantesByTurma(TurmaDTO turma) {
		/*RestTemplate restTemplate = new RestTemplate();
		String url = config.getUrlBase() + "turma/" + config.getVersao() + "/participantes?id-turma=" + turma.getIdTurma();
		HttpEntity<String> requestEntity = new HttpEntity<String>(getHeaders());
		ResponseEntity<ParticipanteTurmaDTO[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ParticipanteTurmaDTO[].class);
		return Arrays.asList(responseEntity.getBody());*/
		return null;
	}
	
}
