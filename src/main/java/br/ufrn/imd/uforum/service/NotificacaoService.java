package br.ufrn.imd.uforum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufframework.domain.Post;
import br.ufframework.domain.RespostaPost;
import br.ufframework.domain.TipoDeNotificador;
import br.ufframework.notificacao.service.AbstractNotificacaoService;
import br.ufframework.notificacao.service.NotificacaoFactory;
import br.ufframework.notificacao.service.Notificador;
import br.ufframework.telegram.dto.MessageDTO;

@Service
public class NotificacaoService extends AbstractNotificacaoService {
	
	@Autowired
	private NotificacaoFactory notificacaoFactory;

	@Override
	public void criarNotificacaoPostCadastrado(Post post) {
		if (post == null || post.getGrupo() == null)
			return;
		
		String text = "Novo post cadastrado em " +
					post.getGrupo().getTitulo() +
					"!\n\n<b>Título</b>: " + 
					post.getTitulo();
		MessageDTO message = new MessageDTO();    	
    	message.setText(text);
    	notificacao = message;
	}

	@Override
	public void criarNotificacaoPostRespondido(RespostaPost resposta) {
		if (resposta == null || resposta.getPost() == null || resposta.getPost().getGrupo() == null)
			return;
		
		String text = "O post " +
				resposta.getPost().getTitulo() +
				"do grupo <b>" +
				resposta.getPost().getGrupo().getTitulo() +
				"</b> foi respondido!\n\n<i>" +
				resposta.getConteudo() +
				"</i>";
		MessageDTO message = new MessageDTO();    	
		message.setText(text);
		notificacao = message;
	}

	@Override
	public Notificador getNotificador() {
		return notificacaoFactory.getNotificador(TipoDeNotificador.TELEGRAM);
	}

}
