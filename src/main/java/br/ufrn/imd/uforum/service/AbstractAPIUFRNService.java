package br.ufrn.imd.uforum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import br.ufrn.imd.uforum.config.APIUFRNConfig;
import br.ufrn.imd.uforum.dto.AccessTokenDTO;

/**
 * Classe que deve ser implementada pelos services da aplicação
 * 
 * @author Lucas Aléssio
 *
 */
public abstract class AbstractAPIUFRNService {

	@Autowired
	protected APIUFRNConfig config;
	
	private AccessTokenDTO accessToken = new AccessTokenDTO();
	
	public HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "bearer " + accessToken.getToken());
		headers.add("x-api-key", config.getxApiKey());
		return headers;
	}
	
	public void setAccessToken(String token) {
		accessToken = new AccessTokenDTO();
		accessToken.setToken(token);
	}

	public void setAccessToken(AccessTokenDTO accessToken) {
		this.accessToken = accessToken;
	}

	public AccessTokenDTO getAccessToken() {
		return accessToken;
	}
	
}
