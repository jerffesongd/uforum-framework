package br.ufrn.imd.uforum.service;

import java.util.Comparator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufframework.domain.Post;
import br.ufframework.repository.PostRepository;
import br.ufframework.service.AbstractGeraRelatorio;
import br.ufrn.imd.uforum.controller.PostController;

@Service
public class GeraRelatorio implements AbstractGeraRelatorio{

	@Autowired
	PostRepository postRepository;
	
	public GeraRelatorio() {
	}
	
	@Override
	public Map<String, Integer> gerar() {
		Map<String, Integer> valores = new LinkedHashMap<String, Integer>();
		
		List<Post>  posts = postRepository.findAll();
		posts.sort(new PostComparator());
		
		for(Post post : posts) {
			valores.put(post.getTitulo(), post.getVisualizacoes());
		}	
		
		//List<Post>  posts = postRepository.findAll();
		
//		valores.put("Post de Teste 1", 8);
//		valores.put("Post de Teste 2", 4);
//		valores.put("Post de Teste 3", 11);
//		valores.put("Post de Teste 4", 13);
//		valores.put("Post de Teste 5", 14);
//		valores.put("Post de Teste 6", 11);
//		valores.put("Post de Teste 7", 11);
//		valores.put("Post de Teste 8", 11);
//		valores.put("Post de Teste 9", 11);
//		valores.put("Post de Teste 10", 11);
//		valores.put("Post de Teste 11", 11);
//		valores.put("Post de Teste 12", 11);
//		valores.put("Post de Teste 13", 11);
//		valores.put("Post de Teste 15", 11);
//		valores.put("Post de Teste 15", 11);
//		valores.put("Post de Teste 16", 11);
//		valores.put("Post de Teste 17", 11);
//		valores.put("Post de Teste 18", 11);
//		valores.put("Post de Teste 19", 11);
//		valores.put("Post de Teste 20", 11);
//		valores.put("Post de Teste 21", 11);
			
		return valores;
	}

}

class PostComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		Post post1 = (Post) o1;
		Post post2 = (Post) o2;
		if(post1.getVisualizacoes() == post2.getVisualizacoes()) {
			return 0;
		}else if(post1.getVisualizacoes() < post2.getVisualizacoes()) {
			return 1;
		}else {
			return -1;
		}
	}
}