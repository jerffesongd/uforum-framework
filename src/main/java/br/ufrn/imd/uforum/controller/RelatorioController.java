package br.ufrn.imd.uforum.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.controller.AbstractPostController;
import br.ufframework.controller.AbstractRelatorioController;
import br.ufframework.domain.Post;
import br.ufframework.service.AbstractGeraRelatorio;
import br.ufrn.imd.uforum.repository.TurmaDTORepository;
import br.ufrn.imd.uforum.service.GeraRelatorio;

@Controller
@RequestMapping("/relatorio")
public class RelatorioController extends AbstractRelatorioController{
	@Autowired
	GeraRelatorio geraRelatorio;
	//Adiciona Uma classe com o algoritmo para gerar relatorio
	public RelatorioController() {
	}
	
	@Override
	public ModelAndView mostrarRelatorio(){
		super.setGeraRelatorio(geraRelatorio);
		return super.mostrarRelatorio();
	}
	
}

