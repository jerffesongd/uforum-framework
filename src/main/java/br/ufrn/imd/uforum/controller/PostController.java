package br.ufrn.imd.uforum.controller;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.controller.AbstractPostController;
import br.ufframework.domain.ForumOperacoes;
import br.ufframework.domain.Post;
import br.ufrn.imd.uforum.dto.UsuarioDTO;
import br.ufrn.imd.uforum.repository.TurmaDTORepository;
import br.ufrn.imd.uforum.repository.UsuarioDTORepository;
import br.ufrn.imd.uforum.service.NotificacaoService;

@Controller
@RequestMapping("/post")
public class PostController extends AbstractPostController{
	
	@Autowired
	private NotificacaoService notificacaoService;
	
	@Autowired
	private TurmaDTORepository turmaDTORepository;
	
	@Autowired
	private UsuarioDTORepository usuarioDTORepository;
	
	@Override
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public ModelAndView salvar(Post post, BindingResult result, RedirectAttributes ra) {
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/post/"+idGrupo, true));
		
		if(post.getId() == null) { // Novo post
			
			post.setGrupo(turmaDTORepository.findOne(idGrupo));
			post.setDataCriacao(new Date());
	    	post.setStatus("CADASTRADO");
			
		
		}else {
			this.idPost = 0;
    	}
		
		post.setResponsavel(usuarioHelper.getUsuarioLogado()); // não sei por que ao editar, o valor não vem do form
    	post.setTitulo(removerCaracteresEspeciais(post.getTitulo())); // É necessário devido a limitação do telegram
    	post.setConteudo(removerCaracteresEspeciais(post.getConteudo())); // É necessário devido a limitação do telegram
    	
    	postRepository.save(post);
    	mensagemHelper.sucesso(ra, "msg.salvar.sucesso"); // msg.salvar.sucesso está definido no arquivo message.properties

    	Collection<UsuarioDTO> usuarios = usuarioDTORepository.findUsersByTurma(idGrupo);
    	notificacaoService.notificar(ForumOperacoes.CADASTRAR_POST, post, null, usuarios);
    	
		return modelAndView;
	}

	protected Object getGrupo(Integer idTurma) {
		return turmaDTORepository.findByIdTurma(idTurma);
	}


}

