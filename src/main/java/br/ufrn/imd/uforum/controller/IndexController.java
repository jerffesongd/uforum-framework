package br.ufrn.imd.uforum.controller;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.helper.AbstractUsuarioHelper;

@Controller
public class IndexController {

	@Autowired
	private AbstractUsuarioHelper usuarioHelper;
	
	@GetMapping("/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("autenticacao.html");
		
		if(usuarioHelper.getUsuarioLogado() != null) {
			modelAndView = new ModelAndView(new RedirectView("/turmas", true));
		}
		
		return modelAndView;
	}	
	
	@GetMapping("/turmas")
	public ModelAndView turmas() {
		ModelAndView modelAndView = new ModelAndView("index.html");
		modelAndView.addObject("usuario", usuarioHelper.getUsuarioLogado());
		return modelAndView;
	}
	
}
