package br.ufrn.imd.uforum.api.sinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.controller.AbstractIndexController;
import br.ufframework.helper.AbstractUsuarioHelper;

public class IndexController extends AbstractIndexController{
	
	@Autowired
	private AbstractUsuarioHelper usuarioHelper;
	
	@Override
	public ModelAndView index() {
		
		ModelAndView modelAndView = new ModelAndView("autenticacao.html");
		
		if(usuarioHelper.getUsuarioLogado() != null) {
			modelAndView = new ModelAndView(new RedirectView("/turmas", true));
		}
		
		return modelAndView;
	}
	
	@GetMapping("/turmas")
	public ModelAndView turmas() {
		ModelAndView modelAndView = new ModelAndView("index.html");
		modelAndView.addObject("usuario", usuarioHelper.getUsuarioLogado());
		return modelAndView;
	}

}
