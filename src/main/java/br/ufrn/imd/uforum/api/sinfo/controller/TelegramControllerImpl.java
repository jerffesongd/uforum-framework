package br.ufrn.imd.uforum.api.sinfo.controller;

import org.springframework.web.bind.annotation.RestController;

import br.ufframework.telegram.controller.TelegramController;
import br.ufrn.imd.uforum.service.TelegramMessageHandler;

/**
 * 
 * @author Lucas Alessio
 *
 */
@RestController
public class TelegramControllerImpl extends TelegramController<TelegramMessageHandler> {

}
