package br.ufrn.imd.uforum.api.sinfo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.controller.AbstractAutenticationController;
import br.ufframework.helper.AbstractUsuarioHelper;
import br.ufrn.imd.uforum.config.APIUFRNConfig;
import br.ufrn.imd.uforum.dto.AccessTokenDTO;
import br.ufrn.imd.uforum.dto.DiscenteDTO;
import br.ufrn.imd.uforum.dto.TurmaDTO;
import br.ufrn.imd.uforum.dto.UsuarioDTO;
import br.ufrn.imd.uforum.repository.AccessTokenDTORepository;
import br.ufrn.imd.uforum.repository.DiscenteDTORepository;
import br.ufrn.imd.uforum.repository.TurmaDTORepository;
import br.ufrn.imd.uforum.repository.UsuarioDTORepository;
import br.ufrn.imd.uforum.service.DiscenteService;
import br.ufrn.imd.uforum.service.TurmaService;
import br.ufrn.imd.uforum.service.UsuarioService;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Controller
public class AutenticationController extends AbstractAutenticationController {
	
	@Autowired
	private AbstractUsuarioHelper<UsuarioDTO> usuarioHelper;

	@Autowired
	private APIUFRNConfig apiufrnConfig;
	
	@Autowired
	private DiscenteDTORepository discenteDTORepository;
	
	@Autowired
	private TurmaDTORepository turmaDTORepository;
	
	@Autowired
	private AccessTokenDTORepository accessTokenDTORepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private DiscenteService discenteService;
	
	@Autowired
	private UsuarioDTORepository usuarioDTORepository;
	
	@Autowired
	private TurmaService turmaService;

	@Override
	@GetMapping("/autenticacao")
	public String formAutenticacao(Model model) {
		return "autenticacao";
	}
	
	@PostMapping("/autorizacao")
	public ResponseEntity<Object> redirecionarParaPaginaAutorizacao(HttpSession session) throws URISyntaxException {
		String urlRedirect = "http://localhost:8080/uforum/user_info";
		URI url = new URI(apiufrnConfig.getUrlAutenticacao(urlRedirect));
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(url);
		return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
	}

	public AccessTokenDTO getAccessToken(String code) {
		RestTemplate restTemplate = new RestTemplate();
		String urlRedirect = "http://localhost:8080/uforum/user_info";
		String url = String.format(apiufrnConfig.getUrlToken(code, urlRedirect));
		ResponseEntity<AccessTokenDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, null, AccessTokenDTO.class);
		return responseEntity.getBody();
	}

	@GetMapping("/user_info")
	public ModelAndView getInfoUser(@RequestParam(value = "code") String code, Model model) {
		ModelAndView modelAndView = new ModelAndView("autenticacao.html");

		try {
			
			if (usuarioHelper.getUsuarioLogado() == null) {
				
				AccessTokenDTO token = getAccessToken(code);
				accessTokenDTORepository.save(token);
				
				usuarioService.setAccessToken(token);
				discenteService.setAccessToken(token);
				turmaService.setAccessToken(token);
				
				UsuarioDTO usuarioLogado = usuarioService.getUsuarioLogado();
				usuarioLogado.setAccessToken(token);
				
				usuarioLogado = validarUsuario(usuarioLogado);				
				
				List<DiscenteDTO> vinculosDiscente = discenteService.getDiscentes(usuarioLogado); 
				
				if (!vinculosDiscente.isEmpty()) {

					discenteDTORepository.save(vinculosDiscente);
					usuarioLogado.setTurmas(turmaService.getTurmas(vinculosDiscente.iterator().next()));
					
					for (TurmaDTO t : usuarioLogado.getTurmas()) {
						
						TurmaDTO aux = turmaDTORepository.findByIdTurma(t.getIdTurma());
						
						if(aux == null){
							turmaDTORepository.save(t);
						} else {
							t.setId(aux.getId());
						}
					}
					
				} 
				
				usuarioHelper.setUsuarioLogado(usuarioLogado);
				modelAndView = new ModelAndView(new RedirectView("/turmas", true));
			
			} else {
				return new ModelAndView("autenticacao.html");
			}
		
		} catch (Exception e) {
			System.out.println("\n\n********** Erro no processo de login ****************\n\n\n");
			e.printStackTrace();
		}

		return modelAndView;
	}

	private UsuarioDTO validarUsuario(UsuarioDTO usuarioLogado) {
		
		UsuarioDTO aux = usuarioDTORepository.buscarPorCPF(usuarioLogado.getCpfCnpj());
		
		if(aux == null) {
			usuarioDTORepository.save(usuarioLogado);
			return usuarioLogado;
		} else {
			return aux;
		}
	}


	@GetMapping("/logout")
	public ModelAndView logout() {
		usuarioHelper.setUsuarioLogado(null);
		ModelAndView modelAndView = new ModelAndView(new RedirectView("https://autenticacao.info.ufrn.br/sso-server/logout?service=http://localhost:8080/uforum/"));
		return modelAndView;
	}

	@GetMapping("/conectar/telegram")
	public ModelAndView conectarTelegram() {
		ModelAndView modelAndView = new ModelAndView(new RedirectView("https://telegram.me/UForumBot?start="+usuarioHelper.getUsuarioLogado().getIdUsuario()));
		return modelAndView;
	}

}
