package br.ufrn.imd.uforum.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representa um docente da instituição.
 * 
 * @author Lucas Aléssio
 *
 */
@Entity
@Table(schema="ufrn", name="docente")
public class DocenteDTO {

	@Id
	@Column(name="id_docente")
	@JsonProperty("id-docente")
	private Integer idDocente;

	@JsonProperty("cargo")
	private String cargo;

	@JsonProperty("cpf")
	private String cpf;

	@Column(name="data_admissao")
	@JsonProperty("data-admissao")
	private Long dataAdmissao;

	@JsonProperty("email")
	private String email;

	@Column(name="id_ativo")
	@JsonProperty("id-ativo")
	private Integer idAtivo;

	@Column(name="id_cargo")
	@JsonProperty("id-cargo")
	private Long idCargo;

	@Column(name="id_situacao")
	@JsonProperty("id-situacao")
	private Integer idSituacao;

	@Column(name="id_unidade")
	@JsonProperty("id-unidade")
	private Integer idUnidade;

	@JsonProperty("nome")
	private String nome;

	@Column(name="nome_identificacao")
	@JsonProperty("nome-identificacao")
	private String nomeIdentificacao;

	@Column(name="regime_trabalho")
	@JsonProperty("regime-trabalho")
	private Integer regimeTrabalho;

	@JsonProperty("sexo")
	private String sexo;

	@JsonProperty("siape")
	private Integer siape;

	@JsonProperty("unidade")
	private String unidade;

	public Integer getIdDocente() {
		return idDocente;
	}

	public void setIdDocente(Integer idDocente) {
		this.idDocente = idDocente;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Long dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdAtivo() {
		return idAtivo;
	}

	public void setIdAtivo(Integer idAtivo) {
		this.idAtivo = idAtivo;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Integer getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Integer idSituacao) {
		this.idSituacao = idSituacao;
	}

	public Integer getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Integer idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeIdentificacao() {
		return nomeIdentificacao;
	}

	public void setNomeIdentificacao(String nomeIdentificacao) {
		this.nomeIdentificacao = nomeIdentificacao;
	}

	public Integer getRegimeTrabalho() {
		return regimeTrabalho;
	}

	public void setRegimeTrabalho(Integer regimeTrabalho) {
		this.regimeTrabalho = regimeTrabalho;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getSiape() {
		return siape;
	}

	public void setSiape(Integer siape) {
		this.siape = siape;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

}
