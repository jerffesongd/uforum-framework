package br.ufrn.imd.uforum.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representa uma notícia cadastrada em uma turma dos cursos instituição.
 * 
 * @author Lucas Aléssio
 *
 */
public class NoticiaDTO {

	@JsonProperty("autor")
	private String autor;
	
	@JsonProperty("conteudo")
	private String conteudo;
	
	@JsonProperty("data-cadastro")
	private Long dataCadastro;
	
	@JsonProperty("id-noticia")
	private Long idNoticia;
	
	@JsonProperty("id-turma")
	private Integer idTurma;
	
	@JsonProperty("link-arquivo")
	private String linkArquivo;
	
	@JsonProperty("notificacao")
	private Boolean notificacao;
	
	@JsonProperty("titulo")
	private String titulo;

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Long getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Long dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Long getIdNoticia() {
		return idNoticia;
	}

	public void setIdNoticia(Long idNoticia) {
		this.idNoticia = idNoticia;
	}

	public Integer getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(Integer idTurma) {
		this.idTurma = idTurma;
	}

	public String getLinkArquivo() {
		return linkArquivo;
	}

	public void setLinkArquivo(String linkArquivo) {
		this.linkArquivo = linkArquivo;
	}

	public Boolean getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Boolean notificacao) {
		this.notificacao = notificacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
}
