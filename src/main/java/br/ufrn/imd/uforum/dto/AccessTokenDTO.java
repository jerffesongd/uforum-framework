package br.ufrn.imd.uforum.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Entity
@Table(schema="ufrn", name="token")
public class AccessTokenDTO {

	@Id
	@Column(name="access_token")
	@JsonProperty("access_token")
	private String token;

	@Column(name="token_type")
	@JsonProperty("token_type")
	private String type;

	@Column(name="refresh_token")
	@JsonProperty("refresh_token")
	private String refreshToken;

	@Column(name="expires_in")
	@JsonProperty("expires_in")
	private Long expiresIn;

	@Column(name="scope")
	@JsonProperty("scope")
	private String scope;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
	
}
