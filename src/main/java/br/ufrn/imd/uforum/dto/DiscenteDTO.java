package br.ufrn.imd.uforum.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author jerffeson
 * 
 * Classe que mapeia um discente retornado pela API da Sinfo
 * 
 */
@Entity
@Table(schema="ufrn", name="discente")
public class DiscenteDTO {

	@Id
	@Column(name="id_discente")
	@JsonProperty("id-discente")
	private Integer idDiscente;
	
	@Column(name="ano_ingresso")
	@JsonProperty("ano-ingresso")
	private Integer anoIngresso;

	@Column(name="cpf")
	@JsonProperty("cpf-cnpj")
	private String cpfCnpj;

	@JsonProperty("email")
	private String email;

	@Column(name="id_curso")
	@JsonProperty("id-curso")
	private Integer idCurso;	

	@Column(name="id_situacao_discente")
	@JsonProperty("id-situacao-discente")
	private Integer idSituacao;

	@JsonProperty("matricula")
	private Long matricula;

	@Column(name="nome_curso")
	@JsonProperty("nome-curso")
	private String nomeCurso;

	@Column(name="nome_discente")
	@JsonProperty("nome-discente")
	private String nomeDiscente;

	@Column(name="periodo_ingresso")
	@JsonProperty("periodo-ingresso")
	private Integer periodoIngresso;

	@Column(name="sigla_nivel")
	@JsonProperty("sigla-nivel")
	private String siglaNivel;

	public Integer getIdDiscente() {
		return idDiscente;
	}

	public void setIdDiscente(Integer idDiscente) {
		this.idDiscente = idDiscente;
	}

	public Integer getAnoIngresso() {
		return anoIngresso;
	}

	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(Integer idCurso) {
		this.idCurso = idCurso;
	}

	public Integer getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Integer idSituacao) {
		this.idSituacao = idSituacao;
	}

	public Long getMatricula() {
		return matricula;
	}

	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public String getNomeDiscente() {
		return nomeDiscente;
	}

	public void setNomeDiscente(String nomeDiscente) {
		this.nomeDiscente = nomeDiscente;
	}

	public Integer getPeriodoIngresso() {
		return periodoIngresso;
	}

	public void setPeriodoIngresso(Integer periodoIngresso) {
		this.periodoIngresso = periodoIngresso;
	}

	public String getSiglaNivel() {
		return siglaNivel;
	}

	public void setSiglaNivel(String siglaNivel) {
		this.siglaNivel = siglaNivel;
	}
	
}
