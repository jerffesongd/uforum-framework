package br.ufrn.imd.uforum.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representa um participante de uma turma da instituição
 * 
 * @author Lucas Aléssio
 *
 */
@Entity
@Table(schema="ufrn", name="participante_turma")
public class ParticipanteTurmaDTO {

	/**
	 * Siape do docente, ou matricula do discente.
	 */
	@Id
	@JsonProperty("identificador")
	private Long identificador;

	@Column(name="chave_foto")
	@JsonProperty("chave-foto")
	private String chaveFoto;

	@Column(name="codigo_turma")
	@JsonProperty("codigo-turma")
	private String codigoTurma;

	@Column(name="cpf")
	@JsonProperty("cpf-cnpj")
	private String cpf;

	@JsonProperty("email")
	private String email;

	@Column(name="id_fota")
	@JsonProperty("id-foto")
	private Integer idFoto;

	@Column(name="id_lotacao")
	@JsonProperty("id-lotacao")
	private Integer idLotacao;

	@Column(name="id_participante")
	@JsonProperty("id-participante")
	private Integer idParticipante;

	@Column(name="id_tipo_participante")
	@JsonProperty("id-tipo-participante")
	private Integer idTipoParticipante;

	@Column(name="id_turma")
	@JsonProperty("id-turma")
	private Integer idTurma;

	@JsonProperty("login")
	private String login;

	@JsonProperty("lotacao")
	private String lotacao;

	@JsonProperty("nome")
	private String nome;

	@Column(name="tipo_participante")
	@JsonProperty("tipo-participante")
	private String tipoParticipante;

	public Long getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}

	public String getChaveFoto() {
		return chaveFoto;
	}

	public void setChaveFoto(String chaveFoto) {
		this.chaveFoto = chaveFoto;
	}

	public String getCodigoTurma() {
		return codigoTurma;
	}

	public void setCodigoTurma(String codigoTurma) {
		this.codigoTurma = codigoTurma;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdFoto() {
		return idFoto;
	}

	public void setIdFoto(Integer idFoto) {
		this.idFoto = idFoto;
	}

	public Integer getIdLotacao() {
		return idLotacao;
	}

	public void setIdLotacao(Integer idLotacao) {
		this.idLotacao = idLotacao;
	}

	public Integer getIdParticipante() {
		return idParticipante;
	}

	public void setIdParticipante(Integer idParticipante) {
		this.idParticipante = idParticipante;
	}

	public Integer getIdTipoParticipante() {
		return idTipoParticipante;
	}

	public void setIdTipoParticipante(Integer idTipoParticipante) {
		this.idTipoParticipante = idTipoParticipante;
	}

	public Integer getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(Integer idTurma) {
		this.idTurma = idTurma;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLotacao() {
		return lotacao;
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoParticipante() {
		return tipoParticipante;
	}

	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}

}
