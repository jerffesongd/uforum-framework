package br.ufrn.imd.uforum.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.ufframework.domain.Grupo;

/**
 * @author jerffeson
 *
 * Representa uma turma do sistema da Sinfo
 *
 */
@Entity
@Table(name="grupo")
public class TurmaDTO extends Grupo {

	@Column(name="id_turma")
	@JsonProperty("id-turma")
	private Integer idTurma;

	@Column(name="id_componente")
	@JsonProperty("id-componente")
	private Integer idComponente;

	@Column(name="id_discente")
	@JsonProperty("id-discente")
	private Integer idDiscente;

	@Column(name="nome_componente")
	@JsonProperty("nome-componente")
	private String titulo;
	
	@Column(name="codigo_componente")
	@JsonProperty("codigo-componente")
	private String codigo;

	public Integer getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(Integer idTurma) {
		this.idTurma = idTurma;
	}

	public Integer getIdComponente() {
		return idComponente;
	}

	public void setIdComponente(Integer idComponente) {
		this.idComponente = idComponente;
	}

	public Integer getIdDiscente() {
		return idDiscente;
	}

	public void setIdDiscente(Integer idDiscente) {
		this.idDiscente = idDiscente;
	}

	@Override
	public String getTitulo() {
		return titulo;
	}

	@Override
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String getCodigo() {
		return codigo;
	}

	@Override
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
