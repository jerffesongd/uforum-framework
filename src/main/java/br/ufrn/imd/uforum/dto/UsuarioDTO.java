package br.ufrn.imd.uforum.dto;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.ufframework.domain.Usuario;

/**
 * Representa um usuário dos sistemas da instituição.
 * 
 * @author Lucas Aléssio
 *
 */
@Entity
@Table(name="usuario")
public class UsuarioDTO extends Usuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_USUARIO")
	@SequenceGenerator(name="SEQ_USUARIO", sequenceName="ID_SEQ_USUARIO", allocationSize=1)
	@Column(name="id_usuario")
	private Integer idUsuario;
	
	@Column(name="id_usuario_sigaa")
	@JsonProperty("id-usuario")
	private Integer idUsuarioSigaa;

	@Column(name="chave_foto")
	@JsonProperty("chave-foto")
	private String chaveFoto;

	@Column(name="cpf")
	@JsonProperty("cpf-cnpj")
	private Long cpfCnpj;

	@JsonProperty("email")
	private String email;

	@Column(name="id_foto")
	@JsonProperty("id-foto")
	private Integer idFoto;


	@Column(name="nome_pessoa")
	@JsonProperty("nome-pessoa")
	private String nomePessoa;

	@Column(name="url_foto")
	@JsonProperty("url-foto")
	private String urlFoto;

	@Transient
	private Collection<TurmaDTO> turmas;

	@OneToOne(orphanRemoval=true)
	@JoinColumn(name="access_token", referencedColumnName="access_token")
	private AccessTokenDTO accessToken;

	public Integer getIdUsuario() {
		return idUsuario;
	}
	
	@Override
	public Integer getId() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIdUsuarioSigaa() {
		return idUsuarioSigaa;
	}

	public void setIdUsuarioSigaa(Integer idUsuarioSigaa) {
		this.idUsuarioSigaa = idUsuarioSigaa;
	}

	public String getChaveFoto() {
		return chaveFoto;
	}

	public void setChaveFoto(String chaveFoto) {
		this.chaveFoto = chaveFoto;
	}

	public Long getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(Long cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdFoto() {
		return idFoto;
	}

	public void setIdFoto(Integer idFoto) {
		this.idFoto = idFoto;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public Collection<TurmaDTO> getTurmas() {
		return turmas;
	}

	public void setTurmas(Collection<TurmaDTO> turmas) {
		this.turmas = turmas;
	}

	public AccessTokenDTO getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(AccessTokenDTO accessToken) {
		this.accessToken = accessToken;
	}

}