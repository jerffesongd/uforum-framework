package br.ufrn.imd.uforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="${scan.packages}")
@EnableJpaRepositories({"br.ufframework.telegram.repository", "br.ufrn.imd.uforum.repository", "br.ufframework.repository"})
@EntityScan({"br.ufframework.domain", "br.ufrn.imd.uforum.dto", "br.ufframework.telegram.dto"})
public class UforumApplication {

	public static void main(String[] args) {
		SpringApplication.run(UforumApplication.class, args);
	}

}
