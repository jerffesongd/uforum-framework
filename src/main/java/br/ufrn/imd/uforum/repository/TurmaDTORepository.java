package br.ufrn.imd.uforum.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrn.imd.uforum.dto.TurmaDTO;

public interface TurmaDTORepository extends JpaRepository<TurmaDTO, Integer> {
	
	@Query(value="select t.* from usuario u "
			+ "join ufrn.discente d on (d.cpf = cast(u.cpf as text)) "
			+ "join grupo t on (t.id_discente = d.id_discente) "
			+ "where u.id_usuario = :idUser ", nativeQuery=true)
	public Collection<TurmaDTO> findTurmasByUser(@Param("idUser") Integer idUser);
	
	@Query(value="select t from TurmaDTO t where t.idDiscente = :idDiscente")
	public Collection<TurmaDTO> findTurmasByDiscente(@Param("idDiscente") Integer idDiscente);

	@Query(value="select t from TurmaDTO t where t.idTurma = ?1")
	public TurmaDTO findByIdTurma(Integer idGrupo);
	
}
