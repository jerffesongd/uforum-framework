package br.ufrn.imd.uforum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufrn.imd.uforum.dto.AccessTokenDTO;

public interface AccessTokenDTORepository extends JpaRepository<AccessTokenDTO, String> {

}
