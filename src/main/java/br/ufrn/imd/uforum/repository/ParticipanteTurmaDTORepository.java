package br.ufrn.imd.uforum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufrn.imd.uforum.dto.ParticipanteTurmaDTO;

public interface ParticipanteTurmaDTORepository extends JpaRepository<ParticipanteTurmaDTO, Long> {

}
