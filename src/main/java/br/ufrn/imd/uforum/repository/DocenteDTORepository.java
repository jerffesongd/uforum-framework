package br.ufrn.imd.uforum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.imd.uforum.dto.DocenteDTO;

@Repository
public interface DocenteDTORepository extends JpaRepository<DocenteDTO, Integer> {

}
