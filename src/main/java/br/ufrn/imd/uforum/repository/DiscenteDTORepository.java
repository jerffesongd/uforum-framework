package br.ufrn.imd.uforum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.imd.uforum.dto.DiscenteDTO;

@Repository
public interface DiscenteDTORepository extends JpaRepository<DiscenteDTO, Integer> {

}
