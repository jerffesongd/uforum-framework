package br.ufrn.imd.uforum.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrn.imd.uforum.dto.UsuarioDTO;

public interface UsuarioDTORepository extends JpaRepository<UsuarioDTO, Integer> {

	@Query(value = "select * from usuario u where u.cpf = ?1", nativeQuery=true)
	public UsuarioDTO buscarPorCPF(Long cpf);
	
	@Query(value = "select u.* from usuario u " + 
			"join ufrn.discente d on (d.cpf = cast(u.cpf as text)) " + 
			"join grupo g on (g.id_discente = d.id_discente) " + 
			"where g.id_grupo = :idTurma ", nativeQuery=true)
	public Collection<UsuarioDTO> findUsersByTurma(@Param("idTurma") Integer idTurma);
	
}
